package it.isibit.usersatisfaction;

import android.app.Activity;
import android.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;


import it.isibit.usersatisfaction.fragment.A_Fragment;
import it.isibit.usersatisfaction.fragment.B_Fragment;
import it.isibit.usersatisfaction.fragment.C_Fragment;
import it.isibit.usersatisfaction.fragment.D_Fragment;
import it.isibit.usersatisfaction.fragment.E_Fragment;
import it.isibit.usersatisfaction.fragment.F_Fragment;
import it.isibit.usersatisfaction.fragment.G_Fragment;
import it.isibit.usersatisfaction.fragment.H_Fragment;
import it.isibit.usersatisfaction.fragment.I_Fragment;
import it.isibit.usersatisfaction.fragment.L_Fragment;
import it.isibit.usersatisfaction.fragment.end_Fragment;
import it.isibit.usersatisfaction.fragment.errore_Fragment;
import it.isibit.usersatisfaction.fragment.start_Fragment;

/**
 * Created by A on 14/10/2016.
 */

public class FlowManager extends Fragment{
    private static MainActivity mainActivity = new MainActivity();
    private static Bundle bundles;
    public static void open(@Nullable android.app.FragmentManager fragmentManager, @Nullable android.app.Fragment fragment, @Nullable String fragmentName){

        if(fragment instanceof A_Fragment) {
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment, fragment, fragmentName)
                        .addToBackStack(null)
                        .commit();
        }

        if(fragment instanceof B_Fragment) {
            fragmentManager.beginTransaction()
                    .replace(R.id.fragment, fragment, fragmentName)
                    .addToBackStack(null)
                    .commit();
        }

        if(fragment instanceof C_Fragment) {
            fragmentManager.beginTransaction()
                    .replace(R.id.fragment, fragment, fragmentName)
                    .addToBackStack(null)
                    .commit();
        }

        if(fragment instanceof D_Fragment) {
            fragmentManager.beginTransaction()
                    .replace(R.id.fragment, fragment, fragmentName)
                    .addToBackStack(null)
                    .commit();
        }

        if(fragment instanceof E_Fragment) {
            fragmentManager.beginTransaction()
                    .replace(R.id.fragment, fragment, fragmentName)
                    .addToBackStack(null)
                    .commit();
        }

        if(fragment instanceof F_Fragment) {
            fragmentManager.beginTransaction()
                    .replace(R.id.fragment, fragment, fragmentName)
                    .addToBackStack(null)
                    .commit();
        }

        if(fragment instanceof G_Fragment) {
            fragmentManager.beginTransaction()
                    .replace(R.id.fragment, fragment, fragmentName)
                    .addToBackStack(null)
                    .commit();
        }

        if(fragment instanceof H_Fragment) {
            fragmentManager.beginTransaction()
                    .replace(R.id.fragment, fragment, fragmentName)
                    .addToBackStack(null)
                    .commit();
        }

        if(fragment instanceof I_Fragment) {
            fragmentManager.beginTransaction()
                    .replace(R.id.fragment, fragment, fragmentName)
                    .addToBackStack(null)
                    .commit();
        }

        if(fragment instanceof L_Fragment) {
            fragmentManager.beginTransaction()
                    .replace(R.id.fragment, fragment, fragmentName)
                    .addToBackStack(null)
                    .commit();
        }

        if(fragment instanceof end_Fragment) {
            fragmentManager.beginTransaction()
                    .replace(R.id.fragment, fragment, fragmentName)
                    .addToBackStack(null)
                    .commit();
        }

        if(fragment instanceof start_Fragment) {
            fragmentManager.beginTransaction()
                    .replace(R.id.fragment, fragment, fragmentName)
                    .addToBackStack(null)
                    .commit();
        }

        if(fragment instanceof errore_Fragment) {
            fragmentManager.beginTransaction()
                    .replace(R.id.fragment, fragment, fragmentName)
                    .addToBackStack(null)
                    .commit();
        }


    }

}
