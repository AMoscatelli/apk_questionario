package it.isibit.usersatisfaction;


import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Point;
import android.app.Fragment;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;

import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.List;

import it.isibit.usersatisfaction.fragment.A_Fragment;
import it.isibit.usersatisfaction.fragment.start_Fragment;

public class MainActivity extends AppCompatActivity {

    private FragmentManager fragmentManager;
    private static LinearLayout linearLayout;
    private static Button b1, b2, b3, b4, b5, b6, b7, b8, b9, b10;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        linearLayout = findViewById(R.id.steps);
        b1 = (Button) findViewById(R.id.layout1);
        b2 = (Button) findViewById(R.id.layout2);
        b3 = (Button) findViewById(R.id.layout3);
        b4 = (Button) findViewById(R.id.layout4);
        b5 = (Button) findViewById(R.id.layout5);
        b6 = (Button) findViewById(R.id.layout6);
        b7 = (Button) findViewById(R.id.layout7);
        b8 = (Button) findViewById(R.id.layout8);
        b9 = (Button) findViewById(R.id.layout9);
        b10 = (Button) findViewById(R.id.layout_end);
        fragmentManager = this.getFragmentManager();




        Fragment  a = new start_Fragment();

        fragmentManager.beginTransaction()
                .replace(R.id.fragment, a,"")
                .addToBackStack(null)
                .commit();
    }

    public static void showSteps(){
        linearLayout.setVisibility(View.VISIBLE);
    }
    public static void hideSteps(){
        linearLayout.setVisibility(View.GONE);
    }


}
