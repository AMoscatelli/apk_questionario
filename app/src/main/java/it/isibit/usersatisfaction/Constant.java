package it.isibit.usersatisfaction;

public class Constant {


    public static String CENTRO_RIABILITATIVO = "1"; //1 = Collevecchio, 2 = Rieti
    public static String CENTRO_RIABILITATIVO_NAME = "Centro riabilitativo Collvecchio";
    public static Integer DELEAY_PAGE = 60000;
    public static Integer END_PAGE = 3000;
    public static String VERY_BAD = "1";
    public static String BAD = "2";
    public static String NEUTRAL = "3";
    public static String GOOD = "4";
    public static String VERY_GOOD = "5";
    public static final String VERY_GOOD_ALL = "100";

    public static String BASE_URL = "https://igea.online/mondo/questionario/receiveDataApp.php";
}
