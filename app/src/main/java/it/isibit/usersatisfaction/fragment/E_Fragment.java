package it.isibit.usersatisfaction.fragment;


import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import it.isibit.usersatisfaction.Constant;
import it.isibit.usersatisfaction.FlowManager;
import it.isibit.usersatisfaction.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class E_Fragment extends Fragment {


    public E_Fragment() {
        // Required empty public constructor
    }


    private ImageView veryBad;
    private ImageView bad;
    private ImageView neutral;
    private ImageView good;
    private ImageView veryGood;
    private String a, b, c, d, e, f, g, h, i, l;
    private Handler mHandler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.e_layout, container, false);
        return  view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = this.getArguments();

        if(bundle != null){
            a = (String) bundle.get("a");
            b = (String) bundle.get("b");
            c = (String) bundle.get("c");
            d = (String) bundle.get("d");
        }

        veryBad = (ImageView) getActivity().findViewById(R.id.verybad_e);
        bad = (ImageView) getActivity().findViewById(R.id.bad_e);
        neutral = (ImageView) getActivity().findViewById(R.id.neutral_e);
        good = (ImageView) getActivity().findViewById(R.id.good_e);
        veryGood = (ImageView) getActivity().findViewById(R.id.verygood_e);

        veryBad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("a",a);
                bundle.putString("b",b);
                bundle.putString("c",c);
                bundle.putString("d",d);
                bundle.putString("e", Constant.VERY_BAD);
                F_Fragment f_fragment = new F_Fragment();
                f_fragment.setArguments(bundle);
                FlowManager.open(getFragmentManager(), f_fragment, "");
            }
        });

        bad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("a",a);
                bundle.putString("b",b);
                bundle.putString("c",c);
                bundle.putString("d",d);
                bundle.putString("e",Constant.BAD);
                F_Fragment f_fragment = new F_Fragment();
                f_fragment.setArguments(bundle);
                FlowManager.open(getFragmentManager(), f_fragment, "");

            }
        });

        neutral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("a",a);
                bundle.putString("b",b);
                bundle.putString("c",c);
                bundle.putString("d",d);
                bundle.putString("e",Constant.NEUTRAL);
                F_Fragment f_fragment = new F_Fragment();
                f_fragment.setArguments(bundle);
                FlowManager.open(getFragmentManager(), f_fragment, "");

            }
        });

        good.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("a",a);
                bundle.putString("b",b);
                bundle.putString("c",c);
                bundle.putString("d",d);
                bundle.putString("e",Constant.GOOD);
                F_Fragment f_fragment = new F_Fragment();
                f_fragment.setArguments(bundle);
                FlowManager.open(getFragmentManager(), f_fragment, "");

            }
        });

        veryGood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {Bundle bundle = new Bundle();
                bundle.putString("a",a);
                bundle.putString("b",b);
                bundle.putString("c",c);
                bundle.putString("d",d);
                bundle.putString("e",Constant.VERY_GOOD);
                F_Fragment f_fragment = new F_Fragment();
                f_fragment.setArguments(bundle);
                FlowManager.open(getFragmentManager(), f_fragment, "");


            }
        });

        //MainActivity.showSteps();
        Button b5 = (Button) getActivity().findViewById(R.id.layout5);
        b5.setBackground(getResources().getDrawable(R.drawable.green_button, null));
        b5.setTextColor(getResources().getColor(R.color.whiteMondRiabilitazione, null));

        mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                FlowManager.open(getFragmentManager(), new start_Fragment(), "");
            }
        },Constant.DELEAY_PAGE);
    }
    @Override
    public void onDestroyView() {
        mHandler.removeCallbacksAndMessages(null);
        super.onDestroyView();

    }

}
