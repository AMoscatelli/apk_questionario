package it.isibit.usersatisfaction.fragment;


import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import it.isibit.usersatisfaction.Constant;
import it.isibit.usersatisfaction.FlowManager;
import it.isibit.usersatisfaction.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class I_Fragment extends Fragment {


    public I_Fragment() {
        // Required empty public constructor
    }


    private ImageView veryBad;
    private ImageView bad;
    private ImageView neutral;
    private ImageView good;
    private ImageView veryGood;private String a, b, c, d, e, f, g, h, i, l;
    private Handler mHandler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.i_layout, container, false);
        return  view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = this.getArguments();

        if(bundle != null){
            a = (String) bundle.get("a");
            b = (String) bundle.get("b");
            c = (String) bundle.get("c");
            d = (String) bundle.get("d");
            e = (String) bundle.get("e");
            f = (String) bundle.get("f");
            g = (String) bundle.get("g");
            h = (String) bundle.get("h");
        }

        veryBad = (ImageView) getActivity().findViewById(R.id.verybad_i);
        bad = (ImageView) getActivity().findViewById(R.id.bad_i);
        neutral = (ImageView) getActivity().findViewById(R.id.neutral_i);
        good = (ImageView) getActivity().findViewById(R.id.good_i);
        veryGood = (ImageView) getActivity().findViewById(R.id.verygood_i);

        veryBad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("a",a);
                bundle.putString("b",b);
                bundle.putString("c",c);
                bundle.putString("d",d);
                bundle.putString("e",e);
                bundle.putString("f",f);
                bundle.putString("g",g);
                bundle.putString("h",h);
                bundle.putString("i", Constant.VERY_BAD);
                L_Fragment l_fragment = new L_Fragment();
                l_fragment.setArguments(bundle);
                FlowManager.open(getFragmentManager(), l_fragment, "");
            }
        });

        bad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("a",a);
                bundle.putString("b",b);
                bundle.putString("c",c);
                bundle.putString("d",d);
                bundle.putString("e",e);
                bundle.putString("f",f);
                bundle.putString("g",g);
                bundle.putString("h",h);
                bundle.putString("i",Constant.BAD);
                L_Fragment l_fragment = new L_Fragment();
                l_fragment.setArguments(bundle);
                FlowManager.open(getFragmentManager(), l_fragment, "");
            }
        });

        neutral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("a",a);
                bundle.putString("b",b);
                bundle.putString("c",c);
                bundle.putString("d",d);
                bundle.putString("e",e);
                bundle.putString("f",f);
                bundle.putString("g",g);
                bundle.putString("h",h);
                bundle.putString("i",Constant.NEUTRAL);
                L_Fragment l_fragment = new L_Fragment();
                l_fragment.setArguments(bundle);
                FlowManager.open(getFragmentManager(), l_fragment, "");
            }
        });

        good.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("a",a);
                bundle.putString("b",b);
                bundle.putString("c",c);
                bundle.putString("d",d);
                bundle.putString("e",e);
                bundle.putString("f",f);
                bundle.putString("g",g);
                bundle.putString("h",h);
                bundle.putString("i",Constant.GOOD);
                L_Fragment l_fragment = new L_Fragment();
                l_fragment.setArguments(bundle);
                FlowManager.open(getFragmentManager(), l_fragment, "");
            }
        });

        veryGood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("a",a);
                bundle.putString("b",b);
                bundle.putString("c",c);
                bundle.putString("d",d);
                bundle.putString("e",e);
                bundle.putString("f",f);
                bundle.putString("g",g);
                bundle.putString("h",h);
                bundle.putString("i",Constant.VERY_GOOD);
                L_Fragment l_fragment = new L_Fragment();
                l_fragment.setArguments(bundle);
                FlowManager.open(getFragmentManager(), l_fragment, "");
            }
        });

        //MainActivity.showSteps();
        Button b9 = (Button) getActivity().findViewById(R.id.layout9);
        b9.setBackground(getResources().getDrawable(R.drawable.green_button, null));
        b9.setTextColor(getResources().getColor(R.color.whiteMondRiabilitazione, null));

        mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                FlowManager.open(getFragmentManager(), new start_Fragment(), "");
            }
        },Constant.DELEAY_PAGE);
    }
    @Override
    public void onDestroyView() {
        mHandler.removeCallbacksAndMessages(null);
        super.onDestroyView();

    }

}
