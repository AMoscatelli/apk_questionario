package it.isibit.usersatisfaction.fragment;


import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import it.isibit.usersatisfaction.Constant;
import it.isibit.usersatisfaction.FlowManager;
import it.isibit.usersatisfaction.MainActivity;
import it.isibit.usersatisfaction.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class D_Fragment extends Fragment {


    public D_Fragment() {
        // Required empty public constructor
    }


    private ImageView veryBad;
    private ImageView bad;
    private ImageView neutral;
    private ImageView good;
    private ImageView veryGood;
    private String a, b, c, d, e, f, g, h, i, l;
    private Handler mHandler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.d_layout, container, false);
        return  view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = this.getArguments();

        if(bundle != null){
            a = (String) bundle.get("a");
            b = (String) bundle.get("b");
            c = (String) bundle.get("c");
        }

        veryBad = (ImageView) getActivity().findViewById(R.id.verybad_d);
        bad = (ImageView) getActivity().findViewById(R.id.bad_d);
        neutral = (ImageView) getActivity().findViewById(R.id.neutral_d);
        good = (ImageView) getActivity().findViewById(R.id.good_d);
        veryGood = (ImageView) getActivity().findViewById(R.id.verygood_d);

        veryBad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("a",a);
                bundle.putString("b",b);
                bundle.putString("c",c);
                bundle.putString("d", Constant.VERY_BAD);
                E_Fragment e_fragment = new E_Fragment();
                e_fragment.setArguments(bundle);
                FlowManager.open(getFragmentManager(), e_fragment, "");
            }
        });

        bad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("a",a);
                bundle.putString("b",b);
                bundle.putString("c",c);
                bundle.putString("d",Constant.BAD);
                E_Fragment e_fragment = new E_Fragment();
                e_fragment.setArguments(bundle);
                FlowManager.open(getFragmentManager(), e_fragment, "");
            }
        });

        neutral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("a",a);
                bundle.putString("b",b);
                bundle.putString("c",c);
                bundle.putString("d",Constant.NEUTRAL);
                E_Fragment e_fragment = new E_Fragment();
                e_fragment.setArguments(bundle);
                FlowManager.open(getFragmentManager(), e_fragment, "");
            }
        });

        good.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("a",a);
                bundle.putString("b",b);
                bundle.putString("c",c);
                bundle.putString("d",Constant.GOOD);
                E_Fragment e_fragment = new E_Fragment();
                e_fragment.setArguments(bundle);
                FlowManager.open(getFragmentManager(), e_fragment, "");
            }
        });

        veryGood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("a",a);
                bundle.putString("b",b);
                bundle.putString("c",c);
                bundle.putString("d",Constant.VERY_GOOD);
                E_Fragment e_fragment = new E_Fragment();
                e_fragment.setArguments(bundle);
                FlowManager.open(getFragmentManager(), e_fragment, "");
            }
        });

        //MainActivity.showSteps();
        Button b4 = (Button) getActivity().findViewById(R.id.layout4);
        b4.setBackground(getResources().getDrawable(R.drawable.green_button, null));
        b4.setTextColor(getResources().getColor(R.color.whiteMondRiabilitazione, null));
        mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                FlowManager.open(getFragmentManager(), new start_Fragment(), "");
            }
        },Constant.DELEAY_PAGE);
    }
    @Override
    public void onDestroyView() {
        mHandler.removeCallbacksAndMessages(null);
        super.onDestroyView();

    }

}
