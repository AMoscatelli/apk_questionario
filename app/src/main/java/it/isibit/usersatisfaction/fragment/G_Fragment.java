package it.isibit.usersatisfaction.fragment;


import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import it.isibit.usersatisfaction.Constant;
import it.isibit.usersatisfaction.FlowManager;
import it.isibit.usersatisfaction.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class G_Fragment extends Fragment {


    public G_Fragment() {
        // Required empty public constructor
    }


    private ImageView veryBad;
    private ImageView bad;
    private ImageView neutral;
    private ImageView good;
    private ImageView veryGood;
    private String a, b, c, d, e, f, g, h, i, l;
    private Handler mHandler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.g_layout, container, false);
        return  view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = this.getArguments();

        if(bundle != null){
            a = (String) bundle.get("a");
            b = (String) bundle.get("b");
            c = (String) bundle.get("c");
            d = (String) bundle.get("d");
            e = (String) bundle.get("e");
            f = (String) bundle.get("f");
        }

        veryBad = (ImageView) getActivity().findViewById(R.id.verybad_g);
        bad = (ImageView) getActivity().findViewById(R.id.bad_g);
        neutral = (ImageView) getActivity().findViewById(R.id.neutral_g);
        good = (ImageView) getActivity().findViewById(R.id.good_g);
        veryGood = (ImageView) getActivity().findViewById(R.id.verygood_g);

        veryBad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("a",a);
                bundle.putString("b",b);
                bundle.putString("c",c);
                bundle.putString("d",d);
                bundle.putString("e",e);
                bundle.putString("f",f);
                bundle.putString("g", Constant.VERY_BAD);
                H_Fragment h_fragment = new H_Fragment();
                h_fragment.setArguments(bundle);
                FlowManager.open(getFragmentManager(), h_fragment, "");

            }
        });

        bad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("a",a);
                bundle.putString("b",b);
                bundle.putString("c",c);
                bundle.putString("d",d);
                bundle.putString("e",e);
                bundle.putString("f",f);
                bundle.putString("g",Constant.BAD);
                H_Fragment h_fragment = new H_Fragment();
                h_fragment.setArguments(bundle);
                FlowManager.open(getFragmentManager(), h_fragment, "");
            }
        });

        neutral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("a",a);
                bundle.putString("b",b);
                bundle.putString("c",c);
                bundle.putString("d",d);
                bundle.putString("e",e);
                bundle.putString("f",f);
                bundle.putString("g",Constant.NEUTRAL);
                H_Fragment h_fragment = new H_Fragment();
                h_fragment.setArguments(bundle);
                FlowManager.open(getFragmentManager(), h_fragment, "");
            }
        });

        good.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("a",a);
                bundle.putString("b",b);
                bundle.putString("c",c);
                bundle.putString("d",d);
                bundle.putString("e",e);
                bundle.putString("f",f);
                bundle.putString("g",Constant.GOOD);
                H_Fragment h_fragment = new H_Fragment();
                h_fragment.setArguments(bundle);
                FlowManager.open(getFragmentManager(), h_fragment, "");
            }
        });

        veryGood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("a",a);
                bundle.putString("b",b);
                bundle.putString("c",c);
                bundle.putString("d",d);
                bundle.putString("e",e);
                bundle.putString("f",f);
                bundle.putString("g",Constant.VERY_GOOD);
                H_Fragment h_fragment = new H_Fragment();
                h_fragment.setArguments(bundle);
                FlowManager.open(getFragmentManager(), h_fragment, "");
            }
        });

        //MainActivity.showSteps();
        Button b7 = (Button) getActivity().findViewById(R.id.layout7);
        b7.setBackground(getResources().getDrawable(R.drawable.green_button, null));
        b7.setTextColor(getResources().getColor(R.color.whiteMondRiabilitazione, null));

        mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                FlowManager.open(getFragmentManager(), new start_Fragment(), "");
            }
        },Constant.DELEAY_PAGE);
    }
    @Override
    public void onDestroyView() {
        mHandler.removeCallbacksAndMessages(null);
        super.onDestroyView();

    }

}
