package it.isibit.usersatisfaction.fragment;


import android.os.Bundle;
import android.app.Fragment;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import it.isibit.usersatisfaction.Constant;
import it.isibit.usersatisfaction.FlowManager;
import it.isibit.usersatisfaction.MainActivity;
import it.isibit.usersatisfaction.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class A_Fragment extends Fragment {


    public A_Fragment() {
        // Required empty public constructor
    }


    private ImageView veryBad;
    private ImageView bad;
    private ImageView neutral;
    private ImageView good;
    private ImageView veryGood;
    private Handler mHandler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.a_layout, container, false);
        return  view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        veryBad = (ImageView) getActivity().findViewById(R.id.verybad);
        bad = (ImageView) getActivity().findViewById(R.id.bad);
        neutral = (ImageView) getActivity().findViewById(R.id.neutral);
        good = (ImageView) getActivity().findViewById(R.id.good);
        veryGood = (ImageView) getActivity().findViewById(R.id.verygood);

        veryBad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("a", Constant.VERY_BAD); // Put anything what you want

                B_Fragment b_fragment = new B_Fragment();
                b_fragment.setArguments(bundle);
                FlowManager.open(getFragmentManager(), b_fragment, "");
            }
        });

        bad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("a",Constant.BAD); // Put anything what you want

                B_Fragment b_fragment = new B_Fragment();
                b_fragment.setArguments(bundle);
                FlowManager.open(getFragmentManager(), b_fragment, "");
            }
        });

        neutral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("a",Constant.NEUTRAL); // Put anything what you want

                B_Fragment b_fragment = new B_Fragment();
                b_fragment.setArguments(bundle);
                FlowManager.open(getFragmentManager(), b_fragment, "");
            }
        });

        good.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("a",Constant.GOOD); // Put anything what you want

                B_Fragment b_fragment = new B_Fragment();
                b_fragment.setArguments(bundle);
                FlowManager.open(getFragmentManager(), b_fragment, "");
            }
        });

        veryGood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("a",Constant.VERY_GOOD_ALL); // Put anything what you want

                end_Fragment end_fragment = new end_Fragment();
                end_fragment.setArguments(bundle);
                FlowManager.open(getFragmentManager(), end_fragment, "");
            }
        });

        MainActivity.showSteps();
        Button b1 = (Button) getActivity().findViewById(R.id.layout1);
        b1.setBackground(getResources().getDrawable(R.drawable.green_button, null));
        b1.setTextColor(getResources().getColor(R.color.whiteMondRiabilitazione, null));

        mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                FlowManager.open(getFragmentManager(), new start_Fragment(), "");
            }
        },Constant.DELEAY_PAGE);
    }


    @Override
    public void onDestroyView() {
        mHandler.removeCallbacksAndMessages(null);
        super.onDestroyView();

    }
}
