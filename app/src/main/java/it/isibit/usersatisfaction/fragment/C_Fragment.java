package it.isibit.usersatisfaction.fragment;


import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import it.isibit.usersatisfaction.Constant;
import it.isibit.usersatisfaction.FlowManager;
import it.isibit.usersatisfaction.MainActivity;
import it.isibit.usersatisfaction.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class C_Fragment extends Fragment {


    public C_Fragment() {
        // Required empty public constructor
    }


    private ImageView veryBad;
    private ImageView bad;
    private ImageView neutral;
    private ImageView good;
    private ImageView veryGood;
    private String a, b, c, d, e, f, g, h, i, l;
    private Handler mHandler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.c_layout, container, false);
        return  view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = this.getArguments();

        if(bundle != null){
            a = (String) bundle.get("a");
            b = (String) bundle.get("b");
        }

        veryBad = (ImageView) getActivity().findViewById(R.id.verybad_c);
        bad = (ImageView) getActivity().findViewById(R.id.bad_c);
        neutral = (ImageView) getActivity().findViewById(R.id.neutral_c);
        good = (ImageView) getActivity().findViewById(R.id.good_c);
        veryGood = (ImageView) getActivity().findViewById(R.id.verygood_c);

        veryBad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("a",a);
                bundle.putString("b",b);
                bundle.putString("c", Constant.VERY_BAD);
                D_Fragment d_fragment = new D_Fragment();
                d_fragment.setArguments(bundle);
                FlowManager.open(getFragmentManager(), d_fragment, "");
            }
        });

        bad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("a",a);
                bundle.putString("b",b);
                bundle.putString("c",Constant.BAD);
                D_Fragment d_fragment = new D_Fragment();
                d_fragment.setArguments(bundle);
                FlowManager.open(getFragmentManager(), d_fragment, "");
            }
        });

        neutral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("a",a);
                bundle.putString("b",b);
                bundle.putString("c",Constant.NEUTRAL);
                D_Fragment d_fragment = new D_Fragment();
                d_fragment.setArguments(bundle);
                FlowManager.open(getFragmentManager(), d_fragment, "");
            }
        });

        good.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("a",a);
                bundle.putString("b",b);
                bundle.putString("c",Constant.GOOD);
                D_Fragment d_fragment = new D_Fragment();
                d_fragment.setArguments(bundle);
                FlowManager.open(getFragmentManager(), d_fragment, "");
            }
        });

        veryGood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("a",a);
                bundle.putString("b",b);
                bundle.putString("c",Constant.VERY_GOOD);
                D_Fragment d_fragment = new D_Fragment();
                d_fragment.setArguments(bundle);
                FlowManager.open(getFragmentManager(), d_fragment, "");
            }
        });

        MainActivity.showSteps();
        Button b3 = (Button) getActivity().findViewById(R.id.layout3);
        b3.setBackground(getResources().getDrawable(R.drawable.green_button, null));
        b3.setTextColor(getResources().getColor(R.color.whiteMondRiabilitazione, null));

        mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                FlowManager.open(getFragmentManager(), new start_Fragment(), "");
            }
        },Constant.DELEAY_PAGE);
    }
    @Override
    public void onDestroyView() {
        mHandler.removeCallbacksAndMessages(null);
        super.onDestroyView();

    }

}
