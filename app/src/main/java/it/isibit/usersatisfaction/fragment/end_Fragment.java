package it.isibit.usersatisfaction.fragment;


import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import it.isibit.usersatisfaction.Constant;
import it.isibit.usersatisfaction.FlowManager;
import it.isibit.usersatisfaction.MainActivity;
import it.isibit.usersatisfaction.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class end_Fragment extends Fragment {


    public end_Fragment() {
        // Required empty public constructor
    }



    private String a, b, c, d, e, f, g, h, i, l;
    private Handler mHandler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.end_layout, container, false);
        return  view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        Bundle bundle = this.getArguments();

        if(bundle != null){
            a = (String) bundle.get("a");
            if(a.compareTo("100")!=0) {
                b = (String) bundle.get("b");
                c = (String) bundle.get("c");
                d = (String) bundle.get("d");
                e = (String) bundle.get("e");
                f = (String) bundle.get("f");
                g = (String) bundle.get("g");
                h = (String) bundle.get("h");
                i = (String) bundle.get("i");
                l = (String) bundle.get("l");
            }else{
                a = Constant.VERY_GOOD;
                b = Constant.VERY_GOOD;
                c = Constant.VERY_GOOD;
                d = Constant.VERY_GOOD;
                e = Constant.VERY_GOOD;
                f = Constant.VERY_GOOD;
                g = Constant.VERY_GOOD;
                h = Constant.VERY_GOOD;
                i = Constant.VERY_GOOD;
                l = Constant.VERY_GOOD;
            }
        }
        System.out.print(a+" - "+b+" - "+c+" - "+d+" - "+e+" - "+f+" - "+g+" - "+h+" - "+i+" - "+l);
        MainActivity.hideSteps();

        double media=(Double.parseDouble(a)+Double.parseDouble(b)+Double.parseDouble(c)+Double.parseDouble(d)+Double.parseDouble(e)+Double.parseDouble(f)+Double.parseDouble(g)+Double.parseDouble(h)+Double.parseDouble(i)+Double.parseDouble(l))/10;

        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(getContext());
        String url =Constant.BASE_URL+"?centro="+ Constant.CENTRO_RIABILITATIVO+"&a="+a+"&b="+b+"&c="+c+"&d="+d+"&e="+e+"&f="+f+"&g="+g+"&h="+h+"&i="+i+"&l="+l+"&media="+String.valueOf(media);

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("ESITO VOLLEY", "inviato il questionario");
                        mHandler = new Handler();
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                FlowManager.open(getFragmentManager(), new start_Fragment(), "");
                            }
                        },Constant.END_PAGE);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                        Log.d("ESITO VOLLEY", "errore dell'invio del questionario"); mHandler = new Handler();
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                FlowManager.open(getFragmentManager(), new errore_Fragment(), "");
                            }
                        },0);



            }
        });

// Add the request to the RequestQueue.
        queue.add(stringRequest);


    }
    @Override
    public void onDestroyView() {
        mHandler.removeCallbacksAndMessages(null);
        super.onDestroyView();

    }



}
