package it.isibit.usersatisfaction.fragment;


import android.support.annotation.NonNull;
import android.app.Fragment;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import it.isibit.usersatisfaction.Constant;
import it.isibit.usersatisfaction.FlowManager;
import it.isibit.usersatisfaction.MainActivity;
import it.isibit.usersatisfaction.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class start_Fragment extends Fragment {


    public start_Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.start_layout, container, false);
        return  view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
       super.onViewCreated(view, savedInstanceState);
       ImageView imageView = (ImageView)  view.findViewById(R.id.logo);
       imageView.setOnLongClickListener(new View.OnLongClickListener() {
           @Override
           public boolean onLongClick(View v) {
               Toast.makeText(getContext(), Constant.CENTRO_RIABILITATIVO_NAME, Toast.LENGTH_SHORT).show();
               return false;

           }
       });
       Button inizio = (Button) view.findViewById(R.id.inizio);
       inizio.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Fragment  a = new A_Fragment();
               FlowManager.open(getFragmentManager(), a,null);
           }
       });

        Button b1 = (Button) getActivity().findViewById(R.id.layout1);
        b1.setBackground(getResources().getDrawable(R.drawable.step_button, null));
        b1.setTextColor(getResources().getColor(R.color.blackMondRiabilitazione, null));

        Button b2 = (Button) getActivity().findViewById(R.id.layout2);
        b2.setBackground(getResources().getDrawable(R.drawable.step_button, null));
        b2.setTextColor(getResources().getColor(R.color.blackMondRiabilitazione, null));

        Button b3 = (Button) getActivity().findViewById(R.id.layout3);
        b3.setBackground(getResources().getDrawable(R.drawable.step_button, null));
        b3.setTextColor(getResources().getColor(R.color.blackMondRiabilitazione, null));

        Button b4 = (Button) getActivity().findViewById(R.id.layout4);
        b4.setBackground(getResources().getDrawable(R.drawable.step_button, null));
        b4.setTextColor(getResources().getColor(R.color.blackMondRiabilitazione, null));

        Button b5 = (Button) getActivity().findViewById(R.id.layout5);
        b5.setBackground(getResources().getDrawable(R.drawable.step_button, null));
        b5.setTextColor(getResources().getColor(R.color.blackMondRiabilitazione, null));

        Button b6 = (Button) getActivity().findViewById(R.id.layout6);
        b6.setBackground(getResources().getDrawable(R.drawable.step_button, null));
        b6.setTextColor(getResources().getColor(R.color.blackMondRiabilitazione, null));

        Button b7 = (Button) getActivity().findViewById(R.id.layout7);
        b7.setBackground(getResources().getDrawable(R.drawable.step_button, null));
        b7.setTextColor(getResources().getColor(R.color.blackMondRiabilitazione, null));

        Button b8 = (Button) getActivity().findViewById(R.id.layout8);
        b8.setBackground(getResources().getDrawable(R.drawable.step_button, null));
        b8.setTextColor(getResources().getColor(R.color.blackMondRiabilitazione, null));

        Button b9 = (Button) getActivity().findViewById(R.id.layout9);
        b9.setBackground(getResources().getDrawable(R.drawable.step_button, null));
        b9.setTextColor(getResources().getColor(R.color.blackMondRiabilitazione, null));

        Button b10 = (Button) getActivity().findViewById(R.id.layout_end);
        b10.setBackground(getResources().getDrawable(R.drawable.step_button, null));
        b10.setTextColor(getResources().getColor(R.color.blackMondRiabilitazione, null));

        MainActivity.hideSteps();

    }
}
