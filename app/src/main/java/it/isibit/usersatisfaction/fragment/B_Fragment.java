package it.isibit.usersatisfaction.fragment;


import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import it.isibit.usersatisfaction.Constant;
import it.isibit.usersatisfaction.FlowManager;
import it.isibit.usersatisfaction.MainActivity;
import it.isibit.usersatisfaction.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class B_Fragment extends Fragment {


    public B_Fragment() {
        // Required empty public constructor
    }


    private ImageView veryBad;
    private ImageView bad;
    private ImageView neutral;
    private ImageView good;
    private ImageView veryGood;
    private String a, b, c, d, e, f, g, h, i, l;
    private Handler mHandler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.b_layout, container, false);
        return  view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle bundle = this.getArguments();

        if(bundle != null){
           a = (String) bundle.get("a");
        }
        veryBad = (ImageView) getActivity().findViewById(R.id.verybad_b);
        bad = (ImageView) getActivity().findViewById(R.id.bad_b);
        neutral = (ImageView) getActivity().findViewById(R.id.neutral_b);
        good = (ImageView) getActivity().findViewById(R.id.good_b);
        veryGood = (ImageView) getActivity().findViewById(R.id.verygood_b);

        veryBad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("a",a); // Put anything what you want
                bundle.putString("b", Constant.VERY_BAD); // Put anything what you want

                C_Fragment c_fragment = new C_Fragment();
                c_fragment.setArguments(bundle);
                FlowManager.open(getFragmentManager(), c_fragment, "");
            }
        });

        bad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("a",a); // Put anything what you want
                bundle.putString("b",Constant.BAD); // Put anything what you want

                C_Fragment c_fragment = new C_Fragment();
                c_fragment.setArguments(bundle);
                FlowManager.open(getFragmentManager(), c_fragment, "");
            }
        });

        neutral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("a",a); // Put anything what you want
                bundle.putString("b",Constant.NEUTRAL); // Put anything what you want

                C_Fragment c_fragment = new C_Fragment();
                c_fragment.setArguments(bundle);
                FlowManager.open(getFragmentManager(), c_fragment, "");
            }
        });

        good.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("a",a); // Put anything what you want
                bundle.putString("b",Constant.GOOD); // Put anything what you want

                C_Fragment c_fragment = new C_Fragment();
                c_fragment.setArguments(bundle);
                FlowManager.open(getFragmentManager(), c_fragment, "");
            }
        });

        veryGood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("a",a); // Put anything what you want
                bundle.putString("b",Constant.VERY_GOOD); // Put anything what you want

                C_Fragment c_fragment = new C_Fragment();
                c_fragment.setArguments(bundle);
                FlowManager.open(getFragmentManager(), c_fragment, "");
            }
        });

        //MainActivity.showSteps();
        Button b2 = (Button) getActivity().findViewById(R.id.layout2);
        b2.setBackground(getResources().getDrawable(R.drawable.green_button, null));
        b2.setTextColor(getResources().getColor(R.color.whiteMondRiabilitazione, null));

        mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                FlowManager.open(getFragmentManager(), new start_Fragment(), "");
            }
        },Constant.DELEAY_PAGE);
    }

    @Override
    public void onDestroyView() {
        mHandler.removeCallbacksAndMessages(null);
        super.onDestroyView();

    }
}
